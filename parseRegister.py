#!/usr/bin/env python
# coding: utf-8

# In[19]:


from lxml import html
import requests
import re

#Configure necessary lists (looping through register)
character = [chr(i) for i in range(ord('a'), ord('z') + 1)]
register = ["personen","orte","einrichtungen"]
HIDListe = []

for regType in register:
    for char in character:
        
        url="https://edition-humboldt.de/register/"+regType+"/index.xql?section="+char.upper()
        #print(url)
        page = requests.get(url)
        tree = html.fromstring(page.content)
        regList  = tree.xpath("//div[contains(@class,'boxInner')]/ul/li/a")
        
        #Parse through each list entry on a specific register page
        for entry in regList:
            #print(entry.attrib["href"])
            
            #exctract HumboldIT 
            regex = r"H[\d]{7}"
            matches = re.finditer(regex, entry.attrib["href"], re.MULTILINE)
            for matchNum, match in enumerate(matches, start=1):
                
                #if HID wasn't seen before, scrape details from page
                if (match[0] in HIDListe) == False:
                    f = open("humboldt.tsv", "a")
                    entryUrl = "https://edition-humboldt.de/"+match[0]
                    entryPage = requests.get(entryUrl)
                    #print(entryPage.encoding)
                    entryTree = html.fromstring(entryPage.content.decode('utf8', 'replace'))

                    h1  = entryTree.xpath("//h1/text()")
                    try:
                        title = h1[1]
                    except IndexError:
                        try:
                            title = h1[0]
                        except IndexError:
                            title = ""

                    gnd = entryTree.xpath("//p[contains(@class,'normID')]/a")
                    gndStr = ""
                    for data in gnd:
                        gndStr = (data.attrib["href"])

                    print(match[0]+"\t"+title+"\t"+gndStr,file=f)
                    f.close()
                    HIDListe.append(match[0])


# In[ ]:




