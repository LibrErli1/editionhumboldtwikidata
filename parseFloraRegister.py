#!/usr/bin/env python
# coding: utf-8

# In[38]:


from lxml import html
import requests
import re

#Configure necessary lists (looping through register)
character = [chr(i) for i in range(ord('a'), ord('z') + 1)]
register = ["pflanzen"]
HIDListe = []

for regType in register:
    for char in character:
        
        url="https://edition-humboldt.de/register/"+regType+"/index.xql?section="+char.upper()
        print(url)
        
        page = requests.get(url)
        tree = html.fromstring(page.content)
        regList  = tree.xpath("//div[contains(@class,'boxInner')]/ul/li/a")
        
        #Parse through each list entry on a specific register page
        for entry in regList:
            #print(entry.attrib["href"])
            
            #exctract plant name 
            regex = r"\?name=(.*?)(\&l=)"
            matches = re.finditer(regex, entry.attrib["href"], re.MULTILINE)
            for matchNum, match in enumerate(matches, start=1):
                #print(match[1])
                #if plant wasn't seen before, scrape details from page
                if (match[1] in HIDListe) == False:
                    f = open("humboldtPflanzen.tsv", "a")
                    entryUrl = "https://edition-humboldt.de/register/pflanzen/detail.xql?name="+match[1]
                    entryPage = requests.get(entryUrl)
                    #print(entryPage.encoding)
                    entryTree = html.fromstring(entryPage.content.decode('utf8', 'replace'))

                    h1  = entryTree.xpath("//h1/text()")
                    try:
                        title = h1[1]
                    except IndexError:
                        try:
                            title = h1[0]
                        except IndexError:
                            title = ""

                    gbif = entryTree.xpath("//a[contains(@href,'gbif.org')]")
                    try:
                        P846 = (gbif[0].attrib["href"])
                    except:
                        P846 = ""
                        
                    eol = entryTree.xpath("//a[contains(@href,'eol.org')]")
                    try:
                        P830 = (eol[0].attrib["href"])
                    except:
                        P830 = ""                        
                    
                    print(match[1]+"\t"+h1[1]+"\t"+P846+"\t"+P830, file = f)
                    
                    f.close()
                    HIDListe.append(match[1])


# In[ ]:




